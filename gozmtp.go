package gozmtp

import (
	"errors"
	"io"
	"time"
)

const (
	zmtpVersionMajor     byte          = 3
	zmtpVersionMinor     byte          = 0
	zmtpHandshakeTimeout time.Duration = time.Millisecond * 100
)

var (
	ErrBadProto   = errors.New("bad protocol")
	ErrBadVersion = errors.New("bad version")
	ErrTimeout    = errors.New("timeout")
)

func readWithTimeout(r io.Reader, b []byte) (n int, err error) {
	ch := make(chan bool)
	n = 0
	err = nil

	go func() {
		n, err = r.Read(b)
		ch <- true
	}()

	select {
	case <-ch:
		return
	case <-time.After(zmtpHandshakeTimeout):
		return 0, ErrTimeout
	}

	return
}

func writeWithTimeout(w io.Writer, b []byte) (n int, err error) {
	ch := make(chan bool)
	n = 0
	err = nil

	go func() {
		n, err = w.Write(b)
		ch <- true
	}()

	select {
	case <-ch:
		return
	case <-time.After(zmtpHandshakeTimeout):
		return 0, ErrTimeout
	}

	return
}

func handshake(rw io.ReadWriter, b []byte) (total int, err error) {
	total = 0

	_, err = sendSignature(rw)
	if err != nil {
		return
	}

	n, err := readWithTimeout(rw, b[:1])
	total += n
	if err != nil {
		return
	}

	if b[0] == 0xff {
		n, err = readWithTimeout(rw, b[1:10])
		total += n
		if err != nil {
			return
		}
	} else {
		err = ErrBadProto
		return
	}

	_, err = sendMajorVersion(rw)
	if err != nil {
		return
	}

	n, err = readWithTimeout(rw, b[10:11])
	total += n
	if err != nil {
		return
	}

	if b[10] != 0x03 {
		err = ErrBadVersion
		return
	}

	_, err = sendMinorVersion(rw)
	if err != nil {
		return
	}

	n, err = readWithTimeout(rw, b[11:12])
	total += n
	if err != nil {
		return
	}

	if b[12] != 0x00 {
		err = ErrBadVersion
		return
	}
	return
}

func sendSignature(w io.Writer) (int, error) {
	return writeWithTimeout(w, []byte{0xFF, 0, 0, 0, 0, 0, 0, 0, 1, 0x7F})
}

func sendMajorVersion(w io.Writer) (int, error) {
	return writeWithTimeout(w, []byte{zmtpVersionMajor})
}

func sendMinorVersion(w io.Writer) (int, error) {
	return writeWithTimeout(w, []byte{zmtpVersionMinor})
}
