package gozmtp

import "io"

type DealerSock struct {
	rw  io.ReadWriter
	buf []byte
}

func NewDealer() *DealerSock {
	return &DealerSock{
		buf: make([]byte, 255),
	}
}

func (s *DealerSock) AddReadWriter(rw io.ReadWriter) error {
	_, err := handshake(rw, s.buf)
	if err != nil {
		return err
	}

	s.rw = rw
	return nil
}

func (s *DealerSock) Read(b []byte) (int, error) {
	return s.rw.Read(b)
}

func (s *DealerSock) Write(b []byte) (int, error) {
	return s.rw.Write(b)
}
