package gozmtp

import (
	"net"
	"testing"

	"github.com/zeromq/goczmq"
)

func TestInterOp(t *testing.T) {
	router := goczmq.NewSock(goczmq.ROUTER)
	defer router.Destroy()

	_, err := router.Bind("tcp://*:9999")
	if err != nil {
		t.Errorf("router.Bind failed: %s", err)
	}

	conn, err := net.Dial("tcp", "127.0.0.1:9999")
	if err != nil {
		t.Errorf("net.Dial: %s")
	}

	d := NewDealer()
	err = d.AddReadWriter(conn)
	if err != nil {
		t.Errorf("d.AddReaderWriter: %s", err)
	}
}
